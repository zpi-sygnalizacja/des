﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    class BusLane : Lane
    {

         public BusLane(string name) : base(name)
        {
        }

        public BusLane(string name, bool isSource)
            : base(name, isSource)
        {
        }

        public override BitmapImage Image
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Images/bus.png")); }
        }

        public override Color Color
        {
            get { return Colors.Gold; }
        }
    }
}
