﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    public abstract class Lane
    {
        public string Name { get; set; }

        public bool IsSource { get; private set; }

        public List<Lane> DestinationsLanes { get; set; }

        public abstract BitmapImage Image { get; }

        public abstract Color Color { get; }

        public Flow Flow { get; set; }

        public Lights Lights { get; set; }


        public Dictionary<string, double> DestinationsProbabilities { get; set; }

        protected Lane(string name, bool source = true)
        {
            DestinationsLanes = new List<Lane>();
            Name = name;
            IsSource = source;
            DestinationsProbabilities = new Dictionary<string, double>();
            Flow = new Flow();
            Lights = new Lights(name);
        }

        public void UpdateDestinationsProbabilities()
        {
            foreach (var lane in DestinationsLanes.Where(lane => !DestinationsProbabilities.ContainsKey(lane.Name)))
                DestinationsProbabilities.Add(lane.Name, -1);
        }

        public override string ToString()
        {
            return Name;
        }

        public static Lane GetFromDescription(List<string> description)
        {
            bool isSource = description[0].Contains("flow");
            Lane lane;
            List<string> splited = new List<string>(description[0].Split(null));
            string type = splited[2];
            string name = splited[1];
            switch (type)
            {
                case "bicycle_lane":
                    lane = new BicycleLane(name, isSource);
                    break;
                case "bus_lane":
                    lane = new BusLane(name, isSource);
                    break;
                case "tram_lane":
                    lane = new TramLane(name, isSource);
                    break;
                case "crosswalk":
                    lane = new PedestrianLane(name, isSource);
                    break;
                default:
                    lane = new CarLane(name, isSource);
                    break;
            }
            if (isSource)
                lane.Flow = Flow.GetFromDescription(splited.GetRange(3, splited.Count - 3));
            return lane;
        }
    }
}
