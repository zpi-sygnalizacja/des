﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    public class Lights
    {
        public string Id { get; set; }
        public double StopTime { get; set; }
        public double PrepareToStartTime { get; set; }
        public double GoTime { get; set; }
        public double PrepareToStopTime { get; set; }
        public double PhaseShift { get; set; }

        public Lights(string name)
        {
            Id = name;
            StopTime = PrepareToStartTime = PrepareToStopTime = GoTime = 0;
            PhaseShift = -1;
        }

        public override string ToString()
        {
            return string.Format("light {0} {1} {2} {3} {4} {5}", Id, (int)StopTime, (int)PrepareToStartTime, (int)GoTime,
                (int)PrepareToStopTime, (int)PhaseShift);
        }

        public static Lights GetFromDescription(List<string> description)
        {
            return new Lights(description[1])
            {
                StopTime = Convert.ToDouble(description[2], CultureInfo.InvariantCulture),
                PrepareToStartTime = Convert.ToDouble(description[3], CultureInfo.InvariantCulture),
                GoTime = Convert.ToDouble(description[4], CultureInfo.InvariantCulture),
                PrepareToStopTime = Convert.ToDouble(description[5], CultureInfo.InvariantCulture),
                PhaseShift = Convert.ToDouble(description[6], CultureInfo.InvariantCulture)
            };
        }
    }
}
    