﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    class CarLane : Lane
    {
        public CarLane(string name) : base(name)
        {
        }

        public override BitmapImage Image
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Images/car.png")); }
        }
        public CarLane(string name, bool isSource)
            : base(name, isSource)
        {
        }

        public override Color Color
        {
            get { return Colors.DarkSlateGray; }
        }
    }
}
