﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    public class Flow
    {
        public double HotTime { get; set; }
        public double HotStandardDeviation { get; set; }

        public double ColdTime { get; set; }
        public double ColdStandardDeviation { get; set; }

        public double Delay { get; set; }

        public double PeriodTime { get; set; }
        public double PeriodStandardDeviation { get; set; }

        public double DurationTime { get; set; }
        public double DurationStandardDeviation { get; set; }

        public double Size { get; set; }

        public override string ToString()
        {
            string s = "flow ";
            s += string.Format("hot {0} {1} ", HotTime.ToString(CultureInfo.InvariantCulture), HotStandardDeviation.ToString(CultureInfo.InvariantCulture));
            s += string.Format("cold {0} {1} ", ColdTime.ToString(CultureInfo.InvariantCulture), ColdStandardDeviation.ToString(CultureInfo.InvariantCulture));
            s += string.Format("delay {0} ", Delay.ToString(CultureInfo.InvariantCulture));
            s += string.Format("period {0} {1} ", PeriodTime.ToString(CultureInfo.InvariantCulture), PeriodStandardDeviation.ToString(CultureInfo.InvariantCulture));
            s += string.Format("duration {0} {1} ", DurationTime.ToString(CultureInfo.InvariantCulture), DurationStandardDeviation.ToString(CultureInfo.InvariantCulture));
            s += string.Format("size {0}", Size.ToString(CultureInfo.InvariantCulture));
            return s;
        }

        public static Flow GetFromDescription(List<string> description)
        {   
            return new Flow
            {
                HotTime = Convert.ToDouble(description[2], CultureInfo.InvariantCulture),
                HotStandardDeviation = Convert.ToDouble(description[3], CultureInfo.InvariantCulture),
                ColdTime = Convert.ToDouble(description[5], CultureInfo.InvariantCulture),
                ColdStandardDeviation = Convert.ToDouble(description[6], CultureInfo.InvariantCulture),
                Delay = Convert.ToDouble(description[8], CultureInfo.InvariantCulture),
                PeriodTime = Convert.ToDouble(description[10], CultureInfo.InvariantCulture),
                PeriodStandardDeviation = Convert.ToDouble(description[11], CultureInfo.InvariantCulture),
                DurationTime = Convert.ToDouble(description[13], CultureInfo.InvariantCulture),
                DurationStandardDeviation = Convert.ToDouble(description[14], CultureInfo.InvariantCulture),
                Size = Convert.ToDouble(description[16], CultureInfo.InvariantCulture)
            };
        }

        public Flow()
        {
            HotStandardDeviation =
                HotTime =
                    ColdStandardDeviation =
                        ColdTime =
                            Delay =
                                DurationStandardDeviation =
                                    DurationTime = Size = PeriodStandardDeviation = PeriodTime = 0;
        }
    }
}
