﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    class TramLane : Lane
    {
        public TramLane(string name) : base(name)
        {
        }
        public TramLane(string name, bool isSource)
            : base(name, isSource)
        {
        }

        public override BitmapImage Image
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Images/tram.png")); }
        }

        public override Color Color
        {
            get { return Colors.CornflowerBlue; }
        }
    }
}
