﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    class BicycleLane : Lane
    {
        public BicycleLane(string name) : base(name)
        {
        }

        public override BitmapImage Image
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Images/bicycle.png")); }
        }

        public override Color Color
        {
            get { return Colors.DarkSalmon; }
        }

        public BicycleLane(string name, bool isSource)
            : base(name, isSource)
        {
        }
    }
}
