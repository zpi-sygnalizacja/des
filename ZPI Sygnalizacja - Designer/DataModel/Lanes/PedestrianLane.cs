﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPI_Sygnalizacja___Designer.DataModel.Lanes
{
    class PedestrianLane : Lane
    {
        public PedestrianLane(string name) : base(name)
        {
        }
        public PedestrianLane(string name, bool isSource)
            : base(name, isSource)
        {
        }

        public override BitmapImage Image
        {
            get { return new BitmapImage(new Uri("pack://application:,,,/Images/pedestrian.png")); }
        }

        public override Color Color
        {
            get { return Colors.Chocolate; }
        }
    }
}
