﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ZPI_Sygnalizacja___Designer.DataModel.Lanes;

namespace ZPI_Sygnalizacja___Designer.DataModel
{
    public class Direction
    {
        public string Name { get; set; }

        public List<Lane> SourceLanes { get; set; }
        
        public List<Lane> DestinationLanes { get; set; }

        public bool HasCrossing { get { return MainWindow.Random.Next()%2 == 0; } }

        public Direction()
        {
            SourceLanes = new List<Lane>();
            DestinationLanes = new List<Lane>();
            Name = "";
        }

        public Direction(string s)
        {
            SourceLanes = new List<Lane>();
            DestinationLanes = new List<Lane>();
            Name = s;
        }
        public override string ToString()
        {
            return Name;
        }
       
        #region TOMASZA: do generowania pliku

        private string GetDestinationString(Lane sourceLane, Lane destinationLane)
        {
            if (sourceLane is PedestrianLane)
                return "\t\tcross " + destinationLane.Name;
            return string.Format("\t\tdestination {0} {1}", destinationLane.Name, sourceLane.DestinationsProbabilities[destinationLane.Name].ToString(CultureInfo.InvariantCulture));
        }
        public List<string> ErrorsInDirection()
        {
            List<string> errorsList = new List<string>();
            foreach (Lane sourceLane in SourceLanes)
            {
                double prob = 0;
                if (!LightsValid(sourceLane.Lights, sourceLane is PedestrianLane))
                    errorsList.Add(sourceLane.Name + ": światła");
                foreach (KeyValuePair<string, double> destinationsProbability in sourceLane.DestinationsProbabilities)
                {
                    if (destinationsProbability.Value < 0 || destinationsProbability.Value > 1)
                        errorsList.Add(sourceLane.Name + "->" + destinationsProbability.Key + ": zła wartość prawdopodobieństwa = " + destinationsProbability.Value);
                    prob += destinationsProbability.Value;
                }
                if (Math.Abs(prob - 1) > 0.001 && !(sourceLane is PedestrianLane) && sourceLane.DestinationsLanes.Count > 0)
                    errorsList.Add(sourceLane.Name + ": prawdopodobieństwo nie sumuje się do jedynki");
                if (sourceLane.DestinationsLanes.Count == 0)
                    errorsList.Add(sourceLane.Name + ": brak pasa docelowego");

            }
            return errorsList;
        }
        
        public List<string> GetLanesListToFile()
        {
            List<string> list = new List<string>();
            foreach (Lane sourceLane in SourceLanes)
            {
                list.Add(string.Format("\tlane {0} {1} {2}", sourceLane.Name, GetLaneTypeString(sourceLane),
                    sourceLane.Flow));
                list.AddRange(
                    sourceLane.DestinationsLanes.Select(
                        destinationsLane => GetDestinationString(sourceLane, destinationsLane)));
            }
            list.AddRange(
                DestinationLanes.Select(
                    sourceLane => string.Format("\tlane {0} {1}", sourceLane.Name, GetLaneTypeString(sourceLane))));
            return list;
        }        
  
        public List<string> GetLightsList()
        {
            return SourceLanes.Select(sorceLane => sorceLane.Lights.ToString()).ToList();
        }

        private bool LightsValid(Lights l, bool isPedestrian)
        {
            return l.GoTime > 0
                   && l.StopTime > 0
                   && (isPedestrian || l.PrepareToStartTime > 0)
                   && l.PrepareToStopTime > 0
                   && l.PhaseShift >= 0;
        }

        public string GetDirectionId(int index)
        {
            string s;
            switch (index)
            {
                case 0:
                    s = "east";
                    break;
                case 1:
                    s = "north";
                    break;
                case 2:
                    s = "west";
                    break;
                case 3:
                    s = "south";
                    break;
                default:
                    s = "direction" + (index + 1);
                    break;
            }
            return s;
        }

        private string GetLaneTypeString(Lane l)
        {
            if (l is BicycleLane)
                return "bicycle_lane";
            if (l is BusLane)
                return "bus_lane";
            if (l is TramLane)
                return "tram_lane";
            if (l is PedestrianLane)
                return "crosswalk";
            return "car_lane";
        }

        #endregion //TOMASZA; generowanie

        #region PIOTRA: do wczytywania pliku
        public List<Tuple<int, int, Lane>> GetCrossingIndexesList()
        {
            return (from sourceLane in SourceLanes
                where sourceLane.GetType() == typeof (PedestrianLane)
                select GetCrossingIndexes(sourceLane)).ToList();
        }

        private Tuple<int, int, Lane> GetCrossingIndexes(Lane lane)
        {
            List<Lane> sourceLanes = SourceLanes.FindAll(l => l.GetType() != typeof(PedestrianLane));
            int min = sourceLanes.Count + DestinationLanes.Count;
            int max = 0;
            foreach (Lane crossedLane in lane.DestinationsLanes)
            {
                int t = sourceLanes.FindIndex(l => l.Name == crossedLane.Name);
                if (t < 0)
                {
                    t = DestinationLanes.FindIndex(l => l.Name == crossedLane.Name);
                    if (t < 0)
                        return null;
                    t += sourceLanes.Count;
                }
                min = Math.Min(min, t);
                max = Math.Max(max, t + 1);
            }
            return Tuple.Create(min, max, lane);
        }
        
        public static Direction GetFromDescription(List<string> description)
        {
            Direction direction = new Direction
            {
                Name =
                    description[0].Substring(description[0].IndexOf(description[0].Split(null)[2],
                        StringComparison.Ordinal))
            };
            //Console.WriteLine(direction.Name);

            int actualIndex = 1;
            int end = description.Count;
            while (actualIndex < end)
            {
                int v1 = description.FindIndex(actualIndex + 1, line => line.StartsWith("lane"));
                if (v1 < 0)
                    v1 = end;
                int nextIndex = Math.Min(v1, end);
                //foreach (string s in description.GetRange(actualIndex, nextIndex-actualIndex))
                //{
                //    Console.WriteLine(s);
                //}


                Lane l = Lane.GetFromDescription(description.GetRange(actualIndex, nextIndex - actualIndex));
                if (l.IsSource)
                    direction.SourceLanes.Add(l);
                else
                    direction.DestinationLanes.Add(l);
                actualIndex = nextIndex;
            }

            return direction;
        }

        public void TryAddLights(Lights lights)
        {
            foreach (Lane lane in SourceLanes.Where(lane => lane.Name == lights.Id))
                lane.Lights = lights;
        }

        public void TryAddBindings(List<Tuple<string, Lane, double>> bindings)
        {
            string name = bindings[0].Item1;
            foreach (Lane lane in SourceLanes)
            {
                if (name != lane.Name) continue;
                if (!(lane is PedestrianLane))
                    foreach (Tuple<string, Lane, double> binding in bindings)
                    {
                        lane.DestinationsLanes.Add(binding.Item2);
                        lane.DestinationsProbabilities.Add(binding.Item2.Name, binding.Item3);
                    }
                else
                    foreach (Tuple<string, Lane, double> binding in bindings)
                    {
                        lane.DestinationsLanes.Add(MainWindow.MakeLane(binding.Item2, false));
                        lane.DestinationsProbabilities.Add(binding.Item2.Name, 1d/bindings.Count);
                    }
            }
        }

        public Lane GetLane(string name)
        {
            return DestinationLanes.Find(lane => lane.Name == name) ?? SourceLanes.Find(lane => lane.Name == name);
        }
        #endregion //PIOTRA, do wczytywania
    }
}