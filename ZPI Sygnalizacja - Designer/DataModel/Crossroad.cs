﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ZPI_Sygnalizacja___Designer.DataModel.Lanes;

namespace ZPI_Sygnalizacja___Designer.DataModel
{
    public class Crossroad
    {
        public string Name { get; set; }
        public List<Direction> Directions { get; set; }

        public Crossroad(string name)
        {
            Name = name;
            Directions = new List<Direction>();
            NamesList = new List<string>();
        }

        public void AddDirection(Direction direction)
        {
            Directions.Add(direction);
        }

        public List<string> NamesList { get; set; }

        //uzupełnia listę używanych w skrzyżowaniu nazw o podany parametr
        public void AddToNameList(string s)
        {
            NamesList.Add(s);
        }

        //aktualizuje całą listę nazw skrzyżowania o wszystkie nazwy wykorzystywane przez niego
        //używane przy hardodowanym skrzyżowaniu i wczytywaniu z pliku
        private void UpdateNamesList()
        {
            foreach (var dir in Directions)
            {
                NamesList.Add(dir.Name);
                foreach (var destinationLane in dir.DestinationLanes)
                    NamesList.Add(destinationLane.Name);
                foreach (var sourceLane in dir.SourceLanes)
                {
                    sourceLane.UpdateDestinationsProbabilities();
                    NamesList.Add(sourceLane.Name);
                }
            }
        }

        #region TOMASZA: GENEROWANIE PLIKU
        private List<string> GetDescription()
        {
            List<string> list = new List<string> {"intersection " + Name};
            for (int index = 0; index < Directions.Count; index++)
            {
                Direction dir = Directions[index];
                list.Add(string.Format("segment {0} {1}", dir.GetDirectionId(index), dir.Name));
                list.AddRange(dir.GetLanesListToFile());
            }
            foreach (Direction direction in Directions)
                list.AddRange(direction.GetLightsList());
            return list;
        }

        public List<string> CrossroadErrorList()
        {
            List<string> lista = new List<string>();
            foreach (Direction direction in Directions)
            {
                lista.AddRange(direction.ErrorsInDirection());
            }
            return lista;
        }

        #endregion //TOMASZA: GENEROWANIE PLIKU

        #region PIOTRA
        public string GetTextFile()
        {
            return GetDescription().Aggregate("", (current, line) => current + line + Environment.NewLine);
        }

        public static Crossroad GetFromDescription(List<string> description)
        {
            description.RemoveAll(line => line.StartsWith("#") || line.Length == 0);
            //Console.WriteLine("Lines: " + description.Count);
            //Console.WriteLine();
            for (int i = 0; i < description.Count; i++)
                description[i] = description[i].Trim();
            Crossroad crossroad =
                new Crossroad(description[0].Substring(description[0].IndexOf(" ", StringComparison.Ordinal) + 1).Trim());
            //Console.WriteLine(crossroad.Name);
            ReadCrossroad(crossroad, description);
            ReadBindings(crossroad, description);
            crossroad.UpdateNamesList();
            return crossroad;
        }

        private static void ReadCrossroad(Crossroad crossroad, List<string> description)
        {
            int actualIndex = 1;
            int startOfLights = description.FindIndex(line => line.StartsWith("light"));
            if (startOfLights < 0)
                startOfLights = description.Count;
            while (actualIndex < startOfLights)
            {
                int v1 = description.FindIndex(actualIndex + 1, line => line.StartsWith("segment"));
                if (v1 < 0)
                    v1 = startOfLights;
                int nextIndex = Math.Min(v1, startOfLights);
                crossroad.Directions.Add(Direction.GetFromDescription(description.GetRange(actualIndex, nextIndex - actualIndex)));
                actualIndex = nextIndex;
            }
            //Console.WriteLine(startOfLights + "  " + description.Count);
            while (startOfLights < description.Count)
            {
                foreach (Direction direction in crossroad.Directions)
                    direction.TryAddLights(
                        Lights.GetFromDescription(new List<string>(description[startOfLights].Split(null))));
                startOfLights++;
            }
        }

        private static void ReadBindings(Crossroad crossroad, List<string> description)
        {


            //Console.WriteLine();
            //Console.WriteLine();
            //Console.WriteLine();
            //Console.WriteLine();
            int actualIndex = 1;
            int startOfLights = description.FindIndex(line => line.StartsWith("light"));
            if (startOfLights < 0)
                startOfLights = description.Count;
            while (actualIndex < startOfLights)
            {
                int v1 = description.FindIndex(actualIndex + 1, line => line.StartsWith("segment") || line.StartsWith("lane"));
                if (v1 < 0)
                    v1 = startOfLights;
                int nextIndex = Math.Min(v1, startOfLights);
                if (!description[actualIndex].StartsWith("lane") || !description[actualIndex].Contains("flow"))
                {
                    actualIndex = nextIndex;
                    continue;
                }

                string source = description[actualIndex].Split(null)[1];
                //Console.WriteLine(source);
                //Console.WriteLine(actualIndex + " --> " + nextIndex);
                List<Tuple<string, Lane, double>> bindings = new List<Tuple<string, Lane, double>>();
                foreach (var s in description.GetRange(actualIndex + 1, nextIndex - actualIndex-1))
                {
                    //Console.WriteLine(s);
                    Lane destination = crossroad.GetLane(s.Split(null)[1]);
                    double probability = -1;
                    if (s.StartsWith("destination"))
                        probability = Convert.ToDouble(s.Split(null)[2], CultureInfo.InvariantCulture);
                    bindings.Add(Tuple.Create(source, destination, probability));
                }
                foreach (Direction direction in crossroad.Directions)
                    direction.TryAddBindings(bindings);
                actualIndex = nextIndex;
            }
        }

        private Lane GetLane(string name)
        {
            Lane lane = null;
            for (int i = 0; i < Directions.Count && lane == null; i++)
                lane = Directions[i].GetLane(name);
            return lane;
        }

        public void RemoveFromNamesList(string s)
        {
            NamesList.Remove(s);
        }

        public static Crossroad ExampleCrossroad()
        {
            Crossroad crossroad = new Crossroad("Skrzyżowanie");
            #region DodawaniePasów
            crossroad.AddDirection(new Direction("Kierunek prawy"));
            crossroad.AddDirection(new Direction("Kierunek dolny"));
            crossroad.AddDirection(new Direction("Kierunek lewy"));
            crossroad.AddDirection(new Direction("Kierunek gorny"));
            crossroad.Directions[0].SourceLanes.Add(new BicycleLane("P_Pas_wyj_1"));
            crossroad.Directions[0].SourceLanes.Add(new CarLane("P_Pas_wyj_2"));
            crossroad.Directions[0].SourceLanes.Add(new CarLane("P_Pas_wyj_3"));
            crossroad.Directions[0].SourceLanes.Add(new BusLane("P_Pas_wyj_4"));
            crossroad.Directions[0].SourceLanes.Add(new TramLane("P_Pas_wyj_5"));
            crossroad.Directions[0].DestinationLanes.Add(new TramLane("P_Pas_wej_1", false));
            crossroad.Directions[0].DestinationLanes.Add(new BusLane("P_Pas_wej_2", false));
            crossroad.Directions[0].DestinationLanes.Add(new CarLane("P_Pas_wej_3", false));
            crossroad.Directions[0].DestinationLanes.Add(new CarLane("P_Pas_wej_4", false));
            crossroad.Directions[0].DestinationLanes.Add(new BicycleLane("P_Pas_wej_5", false));

            crossroad.Directions[1].SourceLanes.Add(new BicycleLane("D_Pas_wyj_1"));
            crossroad.Directions[1].SourceLanes.Add(new CarLane("D_Pas_wyj_2"));
            crossroad.Directions[1].SourceLanes.Add(new CarLane("D_Pas_wyj_3"));
            crossroad.Directions[1].SourceLanes.Add(new TramLane("D_Pas_wyj_4"));
            crossroad.Directions[1].DestinationLanes.Add(new TramLane("D_Pas_wej_1", false));

            crossroad.Directions[2].SourceLanes.Add(new BicycleLane("L_Pas_wyj_1"));
            crossroad.Directions[2].SourceLanes.Add(new CarLane("L_Pas_wyj_2"));
            crossroad.Directions[2].SourceLanes.Add(new CarLane("L_Pas_wyj_3"));
            crossroad.Directions[2].SourceLanes.Add(new BusLane("L_Pas_wyj_4"));
            crossroad.Directions[2].SourceLanes.Add(new TramLane("L_Pas_wyj_5"));
            crossroad.Directions[2].DestinationLanes.Add(new TramLane("L_Pas_wej_1", false));
            crossroad.Directions[2].DestinationLanes.Add(new BusLane("L_Pas_wej_2", false));
            crossroad.Directions[2].DestinationLanes.Add(new CarLane("L_Pas_wej_3", false));
            crossroad.Directions[2].DestinationLanes.Add(new CarLane("L_Pas_wej_4", false));
            crossroad.Directions[2].DestinationLanes.Add(new BicycleLane("L_Pas_wej_5", false));

            crossroad.Directions[3].SourceLanes.Add(new BicycleLane("G_Pas_wyj_1"));
            crossroad.Directions[3].SourceLanes.Add(new CarLane("G_Pas_wyj_2"));
            crossroad.Directions[3].SourceLanes.Add(new BusLane("G_Pas_wyj_3"));
            crossroad.Directions[3].DestinationLanes.Add(new BusLane("G_Pas_wej_1", false));
            crossroad.Directions[3].DestinationLanes.Add(new BicycleLane("G_Pas_wej_2", false));
            #endregion DodawaniePasów
            #region DodawanieŚwiateł
            crossroad.Directions[0].SourceLanes[0].Lights = new Lights("P_Pas_wyj_1") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[0].SourceLanes[1].Lights = new Lights("P_Pas_wyj_2") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[0].SourceLanes[2].Lights = new Lights("P_Pas_wyj_3") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[0].SourceLanes[3].Lights = new Lights("P_Pas_wyj_4") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[0].SourceLanes[4].Lights = new Lights("P_Pas_wyj_5") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };

            crossroad.Directions[1].SourceLanes[0].Lights = new Lights("D_Pas_wyj_1") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[1].SourceLanes[1].Lights = new Lights("D_Pas_wyj_2") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[1].SourceLanes[2].Lights = new Lights("D_Pas_wyj_3") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[1].SourceLanes[3].Lights = new Lights("D_Pas_wyj_4") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };

            crossroad.Directions[2].SourceLanes[0].Lights = new Lights("L_Pas_wyj_1") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[2].SourceLanes[1].Lights = new Lights("L_Pas_wyj_2") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[2].SourceLanes[2].Lights = new Lights("L_Pas_wyj_3") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[2].SourceLanes[3].Lights = new Lights("L_Pas_wyj_4") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[2].SourceLanes[4].Lights = new Lights("L_Pas_wyj_5") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };

            crossroad.Directions[3].SourceLanes[0].Lights = new Lights("G_Pas_wyj_1") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[3].SourceLanes[1].Lights = new Lights("G_Pas_wyj_2") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            crossroad.Directions[3].SourceLanes[2].Lights = new Lights("G_Pas_wyj_3") { GoTime = 1, PrepareToStartTime = 1, PrepareToStopTime = 1, StopTime = 1, PhaseShift = 1 };
            #endregion DodawanieŚwiateł
            #region DodawaniePowiązań
            crossroad.Directions[0].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[4]);
            crossroad.Directions[0].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[3].DestinationLanes[1]);
            crossroad.Directions[0].SourceLanes[1].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[3]);
            crossroad.Directions[0].SourceLanes[2].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[2]);
            crossroad.Directions[0].SourceLanes[3].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[1]);
            crossroad.Directions[0].SourceLanes[3].DestinationsLanes.Add(crossroad.Directions[3].DestinationLanes[0]);
            crossroad.Directions[0].SourceLanes[4].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[0]);
            crossroad.Directions[0].SourceLanes[4].DestinationsLanes.Add(crossroad.Directions[1].DestinationLanes[0]);

            crossroad.Directions[1].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[4]);
            crossroad.Directions[1].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[4]);
            crossroad.Directions[1].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[3].DestinationLanes[1]);
            crossroad.Directions[1].SourceLanes[1].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[2]);
            crossroad.Directions[1].SourceLanes[1].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[3]);
            crossroad.Directions[1].SourceLanes[1].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[3]);
            crossroad.Directions[1].SourceLanes[2].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[2]);
            crossroad.Directions[1].SourceLanes[3].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[0]);
            crossroad.Directions[1].SourceLanes[3].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[0]);

            crossroad.Directions[2].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[4]);
            crossroad.Directions[2].SourceLanes[1].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[3]);
            crossroad.Directions[2].SourceLanes[2].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[2]);
            crossroad.Directions[2].SourceLanes[3].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[1]);
            crossroad.Directions[2].SourceLanes[3].DestinationsLanes.Add(crossroad.Directions[3].DestinationLanes[0]);
            crossroad.Directions[2].SourceLanes[4].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[0]);
            crossroad.Directions[2].SourceLanes[4].DestinationsLanes.Add(crossroad.Directions[1].DestinationLanes[0]);

            crossroad.Directions[3].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[4]);
            crossroad.Directions[3].SourceLanes[0].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[4]);
            crossroad.Directions[3].SourceLanes[1].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[2]);
            crossroad.Directions[3].SourceLanes[1].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[3]);
            crossroad.Directions[3].SourceLanes[2].DestinationsLanes.Add(crossroad.Directions[2].DestinationLanes[1]);
            crossroad.Directions[3].SourceLanes[2].DestinationsLanes.Add(crossroad.Directions[0].DestinationLanes[1]);
            #endregion DodawaniePowiązań

            foreach (var direction in crossroad.Directions)
                foreach (var sourceLane in direction.SourceLanes)
                    foreach (var destinationsLane in sourceLane.DestinationsLanes)
                        sourceLane.DestinationsProbabilities.Add(destinationsLane.Name,
                            1.0 / sourceLane.DestinationsLanes.Count);


            crossroad.UpdateNamesList();
            return crossroad;
        }


        #endregion //PIOTRA
    }
}
