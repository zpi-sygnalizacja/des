﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ZPI_Sygnalizacja___Designer.DialogBoxes
{
    /// <summary>
    /// Interaction logic for InputStringDialog.xaml
    /// </summary>
    public partial class InputStringDialog
    {
        private readonly List<string> _names;
        private readonly bool _mustBeDouble;
        private readonly bool _spacesAllowed;

        public InputStringDialog(string question, List<string> namesList, string defaultAnswer = "", bool visable = false, bool doub = false, bool spacesAllowed = false)
        {
            _names = namesList;
            _spacesAllowed = spacesAllowed;
            _mustBeDouble = doub;
            InitializeComponent();
            btnDialogOk.IsEnabled = false;
            lblQuestion.Content = question;
            txtAnswer.Text = defaultAnswer;
            txtAnswer.Focus();
            if (!visable)
            {
                TypesBox.Visibility = Visibility.Hidden;
                typeLabel.Visibility = Visibility.Hidden;
            }
            else
                TypesBox.SelectedIndex = 0;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public string Answer
        {
            get { return txtAnswer.Text; }
        }

        private void TxtAnswer_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (_mustBeDouble)
                {
                    try
                    {
                        double isValid = double.Parse(txtAnswer.Text);
                        btnDialogOk.IsEnabled = isValid >= 0;
                    }
                    catch (Exception)
                    {
                        btnDialogOk.IsEnabled = false;
                    }
                }
                else if (txtAnswer.Text.Length > 0 && !_names.Contains(txtAnswer.Text) && (_spacesAllowed || !txtAnswer.Text.Contains(' ')))
                    btnDialogOk.IsEnabled = true;
                else btnDialogOk.IsEnabled = false;
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public string TypeString
        {
            get { return TypesBox.SelectionBoxItem.ToString(); }
        }
    }
}