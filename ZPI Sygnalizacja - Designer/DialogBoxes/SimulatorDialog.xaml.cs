﻿using System;
using System.Collections.Generic;
using System.IO;
using ZPI_Sygnalizacja___Designer.DataModel;

namespace ZPI_Sygnalizacja___Designer.DialogBoxes
{
    /// <summary>
    /// Interaction logic for SimulatorDialog.xaml
    /// </summary>
    public partial class SimulatorDialog
    {
        private readonly Crossroad _crossroad;
        private readonly string _simulator;
        private readonly string _inputPath = "Skrzyzowanie.x";
        private readonly string _outputPath = "Skrzyzowanie.y";
        private readonly string _analyzer = App.Analyzer;

        public SimulatorDialog(Crossroad crossroad, string simulator)
        {
            _crossroad = crossroad;
            _simulator = simulator;
            InitializeComponent();
        }

        private void RunSimulator()
        {
            File.WriteAllText(_inputPath, _crossroad.GetTextFile());
            System.Diagnostics.Process.Start(_simulator, GetArguments());
            Close();
        }

        private string GetArguments()
        {
            string args = "";
            args += "input " + _inputPath;
            args += " output " + _outputPath;
            if (TimeTextBox.Text.Length > 0 && int.Parse(TimeTextBox.Text) > 0)
                args += " simulation_duration " + TimeTextBox.Text;
            if (AnalyzeCheckBox.IsChecked != null && (bool)AnalyzeCheckBox.IsChecked && File.Exists(_analyzer))
                args += " analyze \"" + _analyzer + "\"";
            return args;
        }
        
        private void TimeTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                int isZero = int.Parse(TimeTextBox.Text);
                Button.IsEnabled = isZero >=0;
            }
            catch (Exception)
            {
                Button.IsEnabled = TimeTextBox.Text.Length == 0;
            }
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            RunSimulator();
        }

        private void AnalyzeCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!File.Exists(_analyzer))
            {
                AnalyzeCheckBox.IsChecked = false;
                new ErrorListDialog(new List<string> { "Brak pliku symulatora:", _analyzer }).ShowDialog();
            }
        }
    }
}
