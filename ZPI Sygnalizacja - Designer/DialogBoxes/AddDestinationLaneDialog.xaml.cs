﻿using System;
using System.Collections.Generic;
using System.Windows;
using ZPI_Sygnalizacja___Designer.DataModel;
using ZPI_Sygnalizacja___Designer.DataModel.Lanes;

namespace ZPI_Sygnalizacja___Designer.DialogBoxes
{
    /// <summary>
    /// Interaction logic for AddDestinationLaneDialog.xaml
    /// </summary>
    internal partial class AddDestinationLaneDialog
    {
        private readonly FooViewModel _root;
        private readonly List<Direction> _directions;
        private readonly List<Tuple<int, int>> _resultListOfIndexes;
        private readonly List<int> _indexes;

        internal AddDestinationLaneDialog(List<Direction> directions)
        {
            //exludedDir jeśli wykluczamy zawracanie, to exludedDir jest kierunkiem, który musi zostać wykluczony
            _resultListOfIndexes = new List<Tuple<int, int>>();
            _directions = directions;
            InitializeComponent();
            _root = tree.Items[0] as FooViewModel;
            _indexes = new List<int>();
            for (int i = 0; i < directions.Count; i++)
            {
                _indexes.Add(i);
            }
            Initalize();
            tree.Focus();
        }

        private void Initalize()
        {
            _root.Name = "Skrzyżowanie";
            for (int index = 0; index < _directions.Count; index++)
            {
                var dir = _directions[index];
                if (index >= _root.Children.Count)
                    _root.Children.Add(new FooViewModel(dir.Name));
                else
                    _root.Children[index].Name = dir.Name;
                for (int i = 0; i < dir.DestinationLanes.Count; i++)
                {
                    var scl = dir.DestinationLanes[i];
                    if (scl.GetType() == typeof(PedestrianLane))
                        continue;
                    if (i >= _root.Children[index].Children.Count)
                        _root.Children[index].Children.Add(new FooViewModel(scl.Name, scl.Image));
                    else
                        _root.Children[index].Children[i].Name = scl.Name;
                }
            }

            
            //hmmm
            //List<FooViewModel> toRemove = new List<FooViewModel>();
            //for (int index = 0; index < root.Children.Count; index++)
            //{
            //    var child = root.Children[index];
            //    if (child.Children.Count == 0 || index == _exludedDirectionIndex)
            //        toRemove.Add(child);
            //}
            //foreach (var tr in toRemove)
            //{
            //    if (root.Children.Contains(tr))
            //        _indexes.Remove(root.Children.IndexOf(tr));
            //}
            //foreach (var tr in toRemove)
            //{
            //    root.Children.Remove(tr);
            //}
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            _resultListOfIndexes.Clear();
            for (int index = 0; index < _root.Children.Count; index++)
            {
                var dir = _root.Children[index];
                for (int i = 0; i < dir.Children.Count; i++)
                {
                    if (dir.Children[i].IsChecked ?? false)
                        _resultListOfIndexes.Add(new Tuple<int, int>(_indexes[index], i));
                }
            }
            DialogResult = true;
        }

        public List<Tuple<int, int>> ListOfIndexes
        {
            get { return _resultListOfIndexes; }
        }
    }
}