﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ZPI_Sygnalizacja___Designer.DataModel.Lanes;

namespace ZPI_Sygnalizacja___Designer.DialogBoxes
{
    /// <summary>
    /// Interaction logic for NewPedestrianLineBox.xaml
    /// </summary>
    internal partial class NewPedestrianLineBox
    {
        private readonly FooViewModel _root;
        private readonly List<Lane> _lanes;
        private readonly List<Lane> _destLanes;
        private readonly List<Tuple<bool, int>> _indexes;
        private int _lanesLenght;
        private readonly List<string> _names;
        internal NewPedestrianLineBox(List<Lane> lanes, List<Lane> lanesDest, string name, List<string> namesList)
        {
            _names = namesList;
            _lanes = lanes;
            _destLanes = lanesDest;
            InitializeComponent();
            _root = tree.Items[0] as FooViewModel;
            _indexes = new List<Tuple<bool, int>>();
            _lanesLenght = _lanes.Count;
            //_destLenght = _destLanes.Count;
            Initalize();
            // ReSharper disable once PossibleNullReferenceException
            _root.Name = name;
            btnDialogOk.IsEnabled = false;
            tree.Focus();
        }

        private void Initalize()
        {
            //List<int> toRemove = new List<int>();
            int counter = 0;
            for (int i = 0; i < _lanes.Count; i++)
            {
                var lane = _lanes[i];
                if (lane.GetType() != typeof (PedestrianLane))
                {
                    if (i >= _root.Children.Count)
                        _root.Children.Add(new FooViewModel(lane.Name, lane.Image));
                    else
                        _root.Children[i].Name = lane.Name;
                }
                else
                //toRemove.Add(i);
                    counter++;
            }/*
            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                _lanes.RemoveAt(toRemove[i]);
            }
            toRemove.Clear();*/
            _lanesLenght -= counter;
            //counter = 0;
            for (int i = 0; i < _destLanes.Count; i++)
            {
                var lane = _destLanes[i];
                if (lane.GetType() != typeof (PedestrianLane))
                {
                    if (i + _lanesLenght >= _root.Children.Count)
                        _root.Children.Add(new FooViewModel(lane.Name, lane.Image));
                    else
                        _root.Children[_lanesLenght + i].Name = lane.Name;
                }
                //else
                //toRemove.Add(i);
                    //counter++;
            }
            /*for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                _destLanes.RemoveAt(toRemove[i]);
            }*/
            //_destLenght -= counter;
        }
        private void TxtAnswer_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (txtAnswer.Text.Length > 0 && !_names.Contains(txtAnswer.Text) && !txtAnswer.Text.Contains(' '))
                    btnDialogOk.IsEnabled = true;
                else btnDialogOk.IsEnabled = false;
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            _indexes.Clear();
            for (int index = 0; index < _lanesLenght; index++)
                if (_root.Children[index].IsChecked ?? false)
                    _indexes.Add(new Tuple<bool, int>(true, index));
            for (int index = _lanesLenght; index < _root.Children.Count; index++)
                if (_root.Children[index].IsChecked ?? false)
                    _indexes.Add(new Tuple<bool, int>(false, index - _lanesLenght));
            DialogResult = true;
        }

        public List<Tuple<bool, int>>  ListOfIndexes
        {
            get { return GetIndexes(); }
        }

        private List<Tuple<bool, int>> GetIndexes()
        {
            List<Tuple<bool, int>> rest = new List<Tuple<bool, int>>();
            if (_indexes.Count < 2)
                return _indexes;
            if (_indexes[0].Item1)
            {
                if (_indexes[_indexes.Count - 1].Item1)
                    for (int i = _indexes[0].Item2; i <= _indexes[_indexes.Count - 1].Item2; i++)
                        rest.Add(new Tuple<bool, int>(true, i));
                else
                {
                    for (int i = _indexes[0].Item2; i < _lanesLenght; i++)
                        rest.Add(new Tuple<bool, int>(true, i));
                    for (int i = 0; i <= _indexes[_indexes.Count - 1].Item2; i++)
                        rest.Add(new Tuple<bool, int>(false, i));
                }
            }
            else
                for (int i = _indexes[0].Item2; i <= _indexes[_indexes.Count - 1].Item2; i++)
                    rest.Add(new Tuple<bool, int>(false, i));
            return rest;
        }

        public string Answer
        {
            get { return txtAnswer.Text; }
        }
    }
}