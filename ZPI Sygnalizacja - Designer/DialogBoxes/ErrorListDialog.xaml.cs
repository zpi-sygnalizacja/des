﻿using System;
using System.Collections.Generic;

namespace ZPI_Sygnalizacja___Designer.DialogBoxes
{
    /// <summary>
    /// Interaction logic for ErrorListDialog.xaml
    /// </summary>
    public partial class ErrorListDialog
    {
        public ErrorListDialog(List<string> errors)
        {
            InitializeComponent();
            ErrorsBlock.Text = "";
            foreach (string error in errors)
            {
                ErrorsBlock.Text += error + Environment.NewLine;
            }
        }
    }
}
