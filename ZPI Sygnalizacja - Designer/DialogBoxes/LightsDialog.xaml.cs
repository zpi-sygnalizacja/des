﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;

namespace ZPI_Sygnalizacja___Designer.DialogBoxes
{
    /// <summary>
    /// Interaction logic for LightsDialog.xaml
    /// </summary>
    public partial class LightsDialog
    {
        private readonly bool _isCrosswalk;

        public LightsDialog(double red, double green, double phase, double yellow, double redYellow=0, bool crosswalk = false)
        {
            InitializeComponent();
            btnDialogOk.IsEnabled = false;
            redText.Text = red.ToString(CultureInfo.InvariantCulture);
            greenText.Text = green.ToString(CultureInfo.InvariantCulture);
            phaseText.Text = phase.ToString(CultureInfo.InvariantCulture);
            yellowText.Text = yellow.ToString(CultureInfo.InvariantCulture);
            redYellowText.Text = redYellow.ToString(CultureInfo.InvariantCulture);
            if (crosswalk)
            {
                redYellowText.Visibility = Visibility.Hidden;
                redYellowLabel.Visibility = Visibility.Hidden;
                yellowLabel.Content = "Długość migania światła zielonego [sek]";
                _isCrosswalk = true;
            }
        }
       
        private void TxtAnswer_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (AreTextesFilled())
                try
                {
                    int isZero = int.Parse(redText.Text);
                    int isZerog = int.Parse(greenText.Text);
                    int isZerop = int.Parse(phaseText.Text); // może być 0!
                    int isZeroy = int.Parse(yellowText.Text);
                    int isZerory = int.Parse(redYellowText.Text);
                    if (_isCrosswalk)
                    {
                        if (isZero <= 0 || isZerog <= 0 || isZerop < 0 || isZeroy <= 0)
                            btnDialogOk.IsEnabled = false;
                        else 
                            btnDialogOk.IsEnabled = true;
                    }
                    else
                    {
                        if (isZero <= 0 || isZerog <= 0 || isZerop < 0 || isZeroy <= 0 || isZerory <=0)
                            btnDialogOk.IsEnabled = false ;
                        else 
                            btnDialogOk.IsEnabled = true;
                    }
                }
                catch (Exception)
                {
                    btnDialogOk.IsEnabled = false;
                }
            else
                btnDialogOk.IsEnabled = false;
        }

        private bool AreTextesFilled()
        {
            return redText.Text.Length > 0 && greenText.Text.Length > 0 && 
                phaseText.Text.Length > 0 && yellowText.Text.Length > 0 && redYellowText.Text.Length > 0;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public double RedLightTime
        {
            get { return double.Parse(redText.Text); }
        }
        public double GreenLightTime
        {
            get { return double.Parse(greenText.Text); }
        }
        public double PhaseShiftTime
        {
            get { return double.Parse(phaseText.Text); }
        }

        public double YellowTime
        {
            get { return double.Parse(yellowText.Text); }
        }
        public double RedYellowTime
        {
            get { return double.Parse(redYellowText.Text); }
        }
    }
}
