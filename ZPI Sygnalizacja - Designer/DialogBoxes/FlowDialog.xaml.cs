﻿using System;
using System.Windows;
using System.Windows.Controls;
using ZPI_Sygnalizacja___Designer.DataModel.Lanes;

namespace ZPI_Sygnalizacja___Designer.DialogBoxes
{
    /// <summary>
    /// Interaction logic for FlowDialog.xaml
    /// </summary>
    public partial class FlowDialog
    {
        //public FlowDialog(double ht, double hsd, double ct, double csd, double pt, double psd, double dt, double dsd, double d, double s)
        public FlowDialog(Flow flow)
        {
            InitializeComponent();
            btnDialogOk.IsEnabled = false;
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            hotTimeText.Text = flow.HotTime.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            hotStandardDeviationText.Text = flow.HotStandardDeviation.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            coldTimeText.Text = flow.ColdTime.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            coldStandardDeviationText.Text = flow.ColdStandardDeviation.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            periodTimeText.Text = flow.PeriodTime.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            periodStandardDeviationText.Text = flow.PeriodStandardDeviation.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            durationTimeText.Text = flow.DurationTime.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            durationStandardDeviationText.Text = flow.DurationStandardDeviation.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            sizeText.Text = flow.Size.ToString();
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            delayText.Text = flow.Delay.ToString();
        }
       
        private void TxtAnswer_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            btnDialogOk.IsEnabled = AreAllValid();
        }

        private bool AreAllValid()
        {
            return CheckTextBox(hotTimeText)
                   && CheckTextBox(hotStandardDeviationText)
                   && CheckTextBox(durationTimeText)
                   && CheckTextBox(durationStandardDeviationText)
                   && CheckTextBox(delayText)
                   && CheckTextBox(sizeText)
                   && CheckTextBox(periodStandardDeviationText)
                   && CheckTextBox(periodTimeText)
                   && CheckTextBox(coldStandardDeviationText)
                   && CheckTextBox(coldTimeText);
        }

        private bool CheckTextBox(TextBox t)
        {
            if (t.Text.Length > 0)
            {
                try
                {
                    double d = double.Parse(t.Text);
                    return d >= 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private double GetDouble(string s)
        {
            try
            {
                double a = double.Parse(s);
                return a;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public double HotTime
        {
            get { return GetDouble(hotTimeText.Text); }
        }
        public double HotStandardDeviation 
        {
            get { return GetDouble(hotStandardDeviationText.Text); }
        }
        public double ColdTime 
        {
            get { return GetDouble(coldTimeText.Text); }
        }
        public double ColdStandardDeviation 
        {
            get { return GetDouble(coldStandardDeviationText.Text); }
        }
        public double PeriodTime 
        {
            get { return GetDouble(periodTimeText.Text); }
        }
        public double PeriodStandardDeviation 
        {
            get { return GetDouble(periodStandardDeviationText.Text); }
        }
        public double DurationTime 
        {
            get { return GetDouble(durationTimeText.Text); }
        }
        public double DurationStandardDeviation 
        {
            get { return GetDouble(durationStandardDeviationText.Text); }
        }
        public double Size 
        {
            get { return GetDouble(sizeText.Text); }
        }
        public double Delay
        {
            get { return GetDouble(delayText.Text); }
        }
    }
}