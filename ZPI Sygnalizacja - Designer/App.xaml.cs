﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using ZPI_Sygnalizacja___Designer.DataModel;

namespace ZPI_Sygnalizacja___Designer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public static readonly string SimulatorX64 = "ZPI Sygnalizacja - Symulator_x64.exe";
        public static readonly string SimulatorX86 = "ZPI Sygnalizacja - Symulator_x86.exe";
        public static readonly string Analyzer = "ZPI Sygnalizacja - Analizator.exe";

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            if (e.Args.Length == 1)
            {
                FileInfo file = new FileInfo(e.Args[0]);
                if (file.Exists)
                {
                    Crossroad c;
                    try
                    {
                        c = Crossroad.GetFromDescription(new List<string>(File.ReadAllLines(e.Args[0])));
                    }
                    catch (Exception)
                    {
                        c = Crossroad.ExampleCrossroad();
                    }
                    new MainWindow(c).Show();
                }
            }
            else
            {
                new MainWindow(Crossroad.ExampleCrossroad()).Show();
            }
        }
    }
}
