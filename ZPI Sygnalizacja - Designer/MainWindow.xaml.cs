﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZPI_Sygnalizacja___Designer.DataModel;
using ZPI_Sygnalizacja___Designer.DataModel.Lanes;
using ZPI_Sygnalizacja___Designer.DialogBoxes;

namespace ZPI_Sygnalizacja___Designer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public static Random Random;

        private Crossroad _crossroad;
        private readonly List<Crossroad> _crossroads;
        private const double MaxIconSize = 40;

        private double CWidth
        {
            get { return Visualisation.ActualWidth; }
        }

        private double CHeight
        {
            get { return Visualisation.ActualHeight; }
        }

        public MainWindow(Crossroad crossroad)
        {
            _crossroad = crossroad;
            InitializeComponent();
            Random = new Random();
            _crossroads = new List<Crossroad>();
            CrossroadTree.ItemsSource = _crossroads;
            _crossroads.Add(_crossroad);
        }

        #region PIOTRA

        //Rysuje skrzyżowanie
        private void DrawCrossroad(Crossroad crossroad2)
        {
            int directions = crossroad2.Directions.Count;
            if(directions<2)
                return;
            Visualisation.Children.Clear();
            double radius = directions == 2 ? 0 : CHeight / 4 * (directions == 3 ? 0.8 : 1);
            double width = directions == 2 ? CHeight / 2 : radius * 2 * Math.Tan(Math.PI / directions);
            double angle = 360d / directions;
            double outterRadius = Math.Sqrt(radius * radius + width * width / 4) + 2;
            double alpha = Math.PI * angle / 180;

            int max =
                crossroad2.Directions.Select(
                    direction =>
                        direction.SourceLanes.Count(lane => lane.GetType() != typeof (PedestrianLane)) +
                        direction.DestinationLanes.Count).Concat(new[] {0}).Max();
            double imageSize = width/max;
            imageSize = Math.Min(imageSize, MaxIconSize);

            DrawCentralPlate(directions, outterRadius, alpha);
            int directionNumber = 0;
            foreach (Direction direction in crossroad2.Directions)
                DrawDirection(direction, directionNumber++, radius, width, angle, alpha, outterRadius, imageSize);
        }

        //Rysuje kierunek
        private void DrawDirection(Direction direction, int directionNumber, double radius, double width, double angle,
            double alpha, double outterRadius, double imageSize)
        {
            List<Lane> sourceLanes = direction.SourceLanes.FindAll(lane => lane.GetType() != typeof (PedestrianLane));
            int lanes = sourceLanes.Count + direction.DestinationLanes.Count;
            double totalWidth = 0;
            double height = CWidth;
            double top = CHeight/2 - height - radius;
            int laneNumber = 0;
            foreach (Lane sourceLane in sourceLanes)
            {
                DrawLane(sourceLane, lanes, directionNumber, laneNumber++, width, height, angle, ref totalWidth, top, radius, alpha, imageSize);
                if (laneNumber != sourceLanes.Count)
                    DrawSeparatingLine(directionNumber, (double)laneNumber / lanes, radius, alpha, width, 90, true);
            }
            if (lanes > 0 && laneNumber != lanes && laneNumber > 0)
                DrawSeparatingLine(directionNumber, (double)laneNumber / lanes, radius, alpha, width, 50);
            foreach (Lane destinationLane in direction.DestinationLanes)
            {
                DrawLane(destinationLane, lanes, directionNumber, laneNumber++, width, height, angle, ref totalWidth,
                    top, radius, alpha, imageSize);
                if (laneNumber != lanes)
                    DrawSeparatingLine(directionNumber, (double)laneNumber / lanes, radius, alpha, width, 90, true);
            }
            DrawPavement(directionNumber, radius, alpha, width, false);
            DrawPavement(directionNumber, radius, alpha, width, true);
            List<Tuple<int, int, Lane>> crossings = direction.GetCrossingIndexesList();
            foreach (Tuple<int, int, Lane> crossing in crossings.Where(crossing => crossing != null))
                DrawCrossing(crossing.Item3, directionNumber, crossing.Item1, crossing.Item2, lanes, outterRadius, alpha, width, imageSize);
        }

        //Rysuje pas
        private void DrawLane(Lane lane, int lanes, int directionNumber, int laneNumber, double width, double height,
            double angle, ref double totalWidth, double top, double radius, double alpha, double imageSize)
        {
            double thisWidth = width/lanes;
            Rectangle r2 = new Rectangle
            {
                Fill = new SolidColorBrush(lane.Color),
                Height = height,
                Width = thisWidth,
                RenderTransform = new RotateTransform(directionNumber*angle + 90, width/2 - totalWidth, CHeight/2 - top)
            };
            if (lane.Equals(CrossroadTree.SelectedItem))
            {
                r2.Stroke = Brushes.Blue;
                r2.StrokeThickness = width/50;
            }
            Canvas.SetTop(r2, top);
            Canvas.SetLeft(r2, (CWidth - width)/2 + totalWidth);
            Visualisation.Children.Add(r2);
            totalWidth += thisWidth;

            if (!lane.IsSource)
            {
                r2.ToolTip = lane.Name;
                return;
            }
            string toolTip = lane.DestinationsLanes.Aggregate("",
                (current, destinationsLane) =>
                    current + (destinationsLane.Name + " :\t" + lane.DestinationsProbabilities[destinationsLane.Name]) +
                    "\n");
            r2.ToolTip = lane.Name + "\n" + (lane.DestinationsLanes.Count > 0
                ? "------------\n" + toolTip.Substring(0, toolTip.Length - 1)
                : "Brak pasów docelowych");
            double position = 1 - (double) laneNumber/lanes;
            double startX = CWidth/2 + (radius + imageSize/2)*Math.Cos(directionNumber*alpha) -
                            (width/2 - width*position + thisWidth/2)*Math.Sin(directionNumber*alpha);
            double startY = CHeight/2 + (radius + imageSize/2)*Math.Sin(directionNumber*alpha) +
                            (width/2 - width*position + thisWidth/2)*Math.Cos(directionNumber*alpha);
            imageSize = imageSize*0.8;
            Image image = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/traffic_lights.png")),
                Width = imageSize,
                Height = imageSize,
                ToolTip =
                        string.Format("Czerwone:\t{0:0} s\nCzerwone+Żółte: \t{1:0} s\nZielone:   \t{2:0} s\nŻółte:       \t{3:0} s\nFaza:           \t{4:0} s",
                        lane.Lights.StopTime,
                        lane.Lights.PrepareToStartTime,
                        lane.Lights.GoTime,
                        lane.Lights.PrepareToStopTime,
                        lane.Lights.PhaseShift)
            };
            Canvas.SetTop(image, startY - imageSize/2);
            Canvas.SetLeft(image, startX - imageSize/2);
            Panel.SetZIndex(image, 20);
            Visualisation.Children.Add(image);
        }

        //Rysuje płytę skrzyżowania
        private void DrawCentralPlate(int directions, double outterRadius, double alpha)
        {
            Polygon center = new Polygon
            {
                Fill = new SolidColorBrush(Color.FromRgb(50, 50, 50)),
                SnapsToDevicePixels = true
            };
            for (int i = 0; i < directions; i++)
                center.Points.Add(new Point(CWidth / 2 + outterRadius * Math.Cos(i * alpha + alpha / 2),
                    CHeight / 2 + outterRadius * Math.Sin(i * alpha + alpha / 2)));
            Visualisation.Children.Add(center);
        }

        //Rysuje linię oddzielającą pasy
        private void DrawSeparatingLine(int index, double position, double radius, double alpha, double width, double factor = 80, bool dotted = false)
        {
            position = 1 - position;
            double startX = CWidth/2 + radius*Math.Cos(index*alpha) - (width/2 - width*position)*Math.Sin(index*alpha);
            double startY = CHeight/2 + radius*Math.Sin(index*alpha) + (width/2 - width*position)*Math.Cos(index*alpha);
            double endX = CWidth/2 + CWidth*Math.Cos(index*alpha) - (width/2 - width*position)*Math.Sin(index*alpha);
            double endY = CHeight/2 + CWidth*Math.Sin(index*alpha) + (width/2 - width*position)*Math.Cos(index*alpha);
            Line line = new Line
            {
                Stroke = Brushes.White,
                StrokeThickness = width/factor,

                X1 = startX,
                Y1 = startY,
                X2 = endX,
                Y2 = endY
            };
            if (dotted)
                line.StrokeDashArray = new DoubleCollection(new double[] {5, 5});
            Panel.SetZIndex(line, 10);
            Visualisation.Children.Add(line);
        }

        private void DrawCrossing(Lane lane, int index, int startIndex, int endIndex, int lanes, double outterRadius, double alpha,
            double width, double imageSize)
        {
            if(lane.DestinationsLanes.Count==0)
                return;
            double end = (double) startIndex/lanes;
            double start = (double) (lanes-endIndex)/lanes;

            double thickness = width/5;
            double startX = CWidth/2 + (outterRadius + thickness/2)*Math.Cos(index*alpha) -
                            (width/2 - width*start)*Math.Sin(index*alpha);
            double startY = CHeight/2 + (outterRadius + thickness/2)*Math.Sin(index*alpha) +
                            (width/2 - width*start)*Math.Cos(index*alpha);

            double endX = CWidth/2 + (outterRadius + thickness/2)*Math.Cos(index*alpha) +
                          (width/2 - width*end)*Math.Sin(index*alpha);
            double endY = CHeight/2 + (outterRadius + thickness/2)*Math.Sin(index*alpha) -
                          (width/2 - width*end)*Math.Cos(index*alpha);
            Line line = new Line
            {
                StrokeThickness = thickness,
                StrokeDashArray = new DoubleCollection {0.1, 0.1},
                X1 = startX,
                Y1 = startY,
                X2 = endX,
                Y2 = endY,
                Stroke = lane.Equals(CrossroadTree.SelectedItem) ? Brushes.Red : Brushes.White
            };

            Panel.SetZIndex(line, 15);

            Visualisation.Children.Add(line);

            double posX = (startX + endX)/2;
            double posY = (startY + endY)/2;
            imageSize = imageSize * 0.8;
            Image image = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/pedestrian_lights.png")),
                Width = imageSize,
                Height = imageSize,
                ToolTip =
                    string.Format(
                        "Czerwone:\t{0:0} s\nZielone:   \t{1:0} s\nZielone migające:\t{2:0} s\nFaza:           \t{3:0} s",
                        lane.Lights.StopTime,
                        lane.Lights.GoTime,
                        lane.Lights.PrepareToStopTime,
                        lane.Lights.PhaseShift)
            };
            Canvas.SetTop(image, posY - imageSize / 2);
            Canvas.SetLeft(image, posX - imageSize / 2);
            Panel.SetZIndex(image, 20);
            Visualisation.Children.Add(image);
        }

        //Rysuje chodnik
        private void DrawPavement(int index, double radius, double alpha, double width, bool right)
        {
            int direction = right ? 1 : -1;

            double pavementWidth = width/8;

            double p1X = CWidth/2 + radius*Math.Cos(index*alpha) - width/2*Math.Sin(index*alpha)*direction;
            double p1Y = CHeight/2 + radius*Math.Sin(index*alpha) + width/2*Math.Cos(index*alpha)*direction;
            double p2X = CWidth/2 + CWidth*Math.Cos(index*alpha) - width/2*Math.Sin(index*alpha)*direction;
            double p2Y = CHeight/2 + CWidth*Math.Sin(index*alpha) + width/2*Math.Cos(index*alpha)*direction;
            double p3X = CWidth/2 + CWidth*Math.Cos(index*alpha) -
                         (width/2 + pavementWidth)*Math.Sin(index*alpha)*direction;
            double p3Y = CHeight/2 + CWidth*Math.Sin(index*alpha) +
                         (width/2 + pavementWidth)*Math.Cos(index*alpha)*direction;
            double p4X = CWidth/2 + (radius + pavementWidth/Math.Sin(alpha))*Math.Cos(index*alpha + alpha*direction) +
                         width/2*Math.Sin(index*alpha + alpha*direction)*direction;
            double p4Y = CHeight/2 + (radius + pavementWidth/Math.Sin(alpha))*Math.Sin(index*alpha + alpha*direction) -
                         width/2*Math.Cos(index*alpha + alpha*direction)*direction;
            Polygon pavement = new Polygon
            {
                Fill = new SolidColorBrush(Colors.DarkGray)
            };
            pavement.Points.Add(new Point(p1X, p1Y));
            pavement.Points.Add(new Point(p2X, p2Y));
            pavement.Points.Add(new Point(p3X, p3Y));
            pavement.Points.Add(new Point(p4X, p4Y));
            Visualisation.Children.Add(pavement);
        }

        private void Visualisation_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DrawCrossroad(_crossroad);
        }
        
        private void CrossroadTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            DrawCrossroad(_crossroad);
        }

        private void UruchomButton_Click(object sender, RoutedEventArgs e)
        {
            string filename = Environment.Is64BitOperatingSystem ? App.SimulatorX64 : App.SimulatorX86;
            if (!File.Exists(filename))
            {
                new ErrorListDialog(new List<string> {"Brak pliku symulatora:", filename}).ShowDialog();
                return;
            }
            new SimulatorDialog(_crossroad, filename).ShowDialog();
        }

        #endregion //PIOTRA

        #region WSPÓLNY
        //odświeża widok
        private void RefreshView()
        {
            //odświeża drzewo
            CrossroadTree.Items.Refresh();
            CrossroadTree.UpdateLayout();
            //odświeża widok
            DrawCrossroad(_crossroad);
        }

        //generuje plik dla symulatra
        private void GenerateFile(string file)
        {
            string text = _crossroad.GetTextFile();
            File.WriteAllText(file, text);
        }
        
        //metoda wykonywana po wciśnięciu przycisku Generuj
        private void GenerujButton_OnClick(object sender, RoutedEventArgs e)
        {
            //sprawdzenie poprawności skrzyżowania, by nie generować niepoprawnych plików
            List<string> lista = _crossroad.CrossroadErrorList();
            if (lista.Count > 0)
            {
                ErrorListDialog dialog = new ErrorListDialog(lista);
                dialog.ShowDialog();
            }
            else
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog
                {
                    DefaultExt = ".x",
                    FileName = "Skrzyżowanie",
                    Filter = "Pliki skrzyżowania (*.x)|*.x"
                };
                if (dlg.ShowDialog() == true)
                    GenerateFile(dlg.FileName);
            }
        }

        //metoda wykonywana po wciśnięciu przycisku Wczytaj
        private void WczytajButton_OnClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".x",
                Filter = "Pliki skrzyżowania (*.x)|*.x"
            };
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    _crossroad = Crossroad.GetFromDescription(new List<string>(File.ReadAllLines(dlg.FileName)));
                    _crossroads.Clear();
                    _crossroads.Add(_crossroad);
                    RefreshView();
                }
                catch (Exception)
                {
                    new ErrorListDialog(new List<string>{ "Błąd wczytywania", "Niepoprawny format pliku" }).ShowDialog();
                }
            }
        }
        #endregion //WSPÓŁNY

        #region TOMASZA
        /*
         * TOMASZA: obsługa użytkownika
         */

        #region MENU
        /*
         * METODY MENU
         */

        //dodawanie; z menu (pierwszy menu item z każdego menu)
        private void MenuItem_Add(object sender, RoutedEventArgs e)
        {
            TextBlock g = GetTextBlock(sender);
            if (g != null)
            {
                CrossroadAddition(g);
            }
        }

        //usuwanie; z menu
        private void MenuItem_Remove(object sender, RoutedEventArgs e)
        {
            TextBlock g = GetTextBlock(sender);
            if (g != null)
            {
                try
                {
                    CrossroadRemoval(g);
                }
                catch (Exception)
                {
                    //Console.WriteLine(@"WYJĄTEK PRZY USUWANIU: " + ex.Message);
                }
            }
        }

        //zmiana nazwy; z menu
        private void MenuItem_Rename(object sender, RoutedEventArgs e)
        {
            TextBlock g = GetTextBlock(sender);
            if (g != null)
            {
                try
                {
                    string question = GetRenameQuestion(g);
                    var temp = g.DataContext;
                    InputStringDialog inputDialog;
                    if (temp is Lane)
                        inputDialog = new InputStringDialog(question, _crossroad.NamesList);
                    else
                        inputDialog = new InputStringDialog("Podaj nazwę:", _crossroad.NamesList, spacesAllowed: true);
                    if (inputDialog.ShowDialog() == true)
                    {
                        string name = inputDialog.Answer;
                        if (temp.GetType() == typeof(Crossroad))
                        {
                            _crossroad.Name = name;
                        }
                        else if (temp.GetType() == typeof(Direction))
                        {
                            RemoveNameFromNamesList(((Direction) temp).Name);
                            ((Direction) temp).Name = name;
                            _crossroad.AddToNameList(name);
                        }
                        else if (temp is Lane)
                        {
                            // ReSharper disable once PossibleInvalidCastException
                            RemoveNameFromNamesList(((Lane) temp).Name);
                            // ReSharper disable once PossibleInvalidCastException
                            ((Lane) temp).Name = name;
                            _crossroad.AddToNameList(name);
                        }
                        RefreshView();
                    }
                }
                catch (Exception)
                {
                    //Console.WriteLine(@"WYJĄTEK PRZY ZMIENIANIU NAZWY: " + ex.Message);
                }
            }
        }

        //metoda dodaje nowy pas wyjściowy do kierunku, od razu wybiera się, które pasy do niej wchodzą
        //menuItem: Dodaj pas docelowy
        private void MenuItem_AddDestinationLane(object sender, RoutedEventArgs e)
        {
            TextBlock g = GetTextBlock(sender);
            AddingDestinationLane dialog = new AddingDestinationLane(_crossroad.Directions, _crossroad.NamesList);
            //{w celu wykluczenia zawracacnia, zamień linijki}
            //AddingDestinationLane dialog = new AddingDestinationLane(_crossroad.Directions, _crossroad.NamesList, _crossroad.Directions.IndexOf(g.DataContext as Direction));
            if (dialog.ShowDialog() == true)
            {
                List<Tuple<int, int>> indexesTuples = dialog.ListOfIndexes;
                if (indexesTuples.Count > 0)
                {
                    var temp = g.DataContext as Direction;
                    Lane newLane = GetLane(dialog.TypeString, dialog.Answer, false);//nowy pas wyjściowy
                    _crossroad.AddToNameList(dialog.Answer);
                    // ReSharper disable once PossibleNullReferenceException
                    temp.DestinationLanes.Add(newLane);
                    foreach (var tuple in indexesTuples)
                    {
                        var sourceLane = _crossroad.Directions[tuple.Item1].SourceLanes[tuple.Item2];
                        sourceLane.DestinationsProbabilities.Add(newLane.Name, -1);
                        sourceLane.DestinationsLanes.Add(newLane);
                        //usuwamy przejścia
                        RemoveAllPedestiranLanes(tuple.Item1);
                    }
                }
                RefreshView();
            }
        }

        //dodaje przejście dla pieszych do danego kierunku
        //menuItem: Dodaj przejście dla pieszych
        private void MenuItem_AddZebra(object sender, RoutedEventArgs e)
        {
            TextBlock g = GetTextBlock(sender);
            if (g.DataContext.GetType() == typeof (Direction))
            {
                Direction temp = g.DataContext as Direction;
                // ReSharper disable once PossibleNullReferenceException
                NewPedestrianLineBox dialog = new NewPedestrianLineBox(temp.SourceLanes, temp.DestinationLanes, temp.Name, _crossroad.NamesList);
                if (dialog.ShowDialog() == true)
                {
                    List<Tuple<bool, int>> lines = dialog.ListOfIndexes;
                    if (lines.Count > 0)
                    {
                        Lane newLane = new PedestrianLane(dialog.Answer);
                        _crossroad.AddToNameList(dialog.Answer);
                        var t = _crossroad.Directions[_crossroad.Directions.IndexOf(temp)];
                        foreach (var line in lines)
                        {
                            if (line.Item1)
                            {
                                var tempLane = t.SourceLanes[line.Item2];
                                //newLane.DestinationsLanes.Add(t.SourceLanes[line.Item2]);
                                newLane.DestinationsLanes.Add(MakeLane(tempLane));
                            }
                            else
                            {
                                var tempLane = t.DestinationLanes[line.Item2];
                                newLane.DestinationsLanes.Add(MakeLane(tempLane, false));
                            }

                        }
                        _crossroad.Directions[_crossroad.Directions.IndexOf(temp)].SourceLanes.Add(newLane);
                    }
                    RefreshView();
                }
            }
        }
        
        //metoda ustawia parametry świateł bądź prawdopodobieństwo wybrania danego pasa
        //menuItem: Ustaw paramety
        private void MenuItem_SetLightsOrProb(object sender, RoutedEventArgs e)
        {
            TextBlock g = GetTextBlock(sender);
            if ((g.DataContext as Lane)!=null)
            {
                var temp = (Lane) g.DataContext;
                if (temp.IsSource)
                {
                    Lights lights = temp.Lights;
                    LightsDialog dialog;
                    if (temp is PedestrianLane)
                        dialog = new LightsDialog(lights.StopTime, lights.GoTime, lights.PhaseShift, lights.PrepareToStopTime, 0, true);
                    else
                        dialog = new LightsDialog(lights.StopTime, lights.GoTime, lights.PhaseShift, lights.PrepareToStopTime, lights.PrepareToStartTime);
                    if (dialog.ShowDialog() == true)
                    {
                        lights.StopTime = dialog.RedLightTime;
                        lights.GoTime = dialog.GreenLightTime;
                        lights.PhaseShift = dialog.PhaseShiftTime;
                        lights.PrepareToStopTime = dialog.YellowTime;
                        //if (!(temp is PedestrianLane))
                        lights.PrepareToStartTime = dialog.RedYellowTime;
                        RefreshView();
                    }
                }
                else
                {
                    if (HasSelectedItemGotALaneAsDestinationLane(temp))
                    {
                        Lane selLane = CrossroadTree.SelectedItem as Lane;
                        // ReSharper disable once PossibleNullReferenceException
                        if (!selLane.DestinationsProbabilities.ContainsKey(temp.Name))
                            selLane.DestinationsProbabilities.Add(temp.Name, -1);
                        InputStringDialog dialog =
                            new InputStringDialog("Podaj prawdopodobieństwo wjazdu na pas, jako wartość od 0 do 1.",
                                new List<string>(), selLane.DestinationsProbabilities[temp.Name].ToString(CultureInfo.InvariantCulture), false, true);
                        if (dialog.ShowDialog() == true)
                        {
                            try
                            {
                                double value = double.Parse(dialog.Answer);
                                selLane.DestinationsProbabilities[temp.Name] = value;
                                //Console.WriteLine(selLane.DestinationsProbabilities[temp.Name]);
                                RefreshView();
                            }
                            catch (Exception)
                            {
                                // ignored
                            }
                        }
                    }
                }
            }
        }

        //metoda ustawia parametry fali
        //menuItem: Ustaw falę
        private void MenuItem_SetFlow(object sender, RoutedEventArgs e)
        {
            TextBlock g = GetTextBlock(sender);
            if ((g.DataContext as Lane)!=null)
            {
                var temp = (Lane) g.DataContext;
                if (temp.IsSource)
                {
                    Flow flow = temp.Flow;
                    FlowDialog dialog = new FlowDialog(flow);
                    if (dialog.ShowDialog() == true)
                    {
                        flow.ColdStandardDeviation = dialog.ColdStandardDeviation;
                        flow.ColdTime = dialog.ColdTime;
                        flow.HotStandardDeviation = dialog.HotStandardDeviation;
                        flow.HotTime = dialog.HotTime;
                        flow.DurationStandardDeviation = dialog.DurationStandardDeviation;
                        flow.DurationTime = dialog.DurationTime;
                        flow.Delay = dialog.Delay;
                        flow.PeriodStandardDeviation = dialog.PeriodStandardDeviation;
                        flow.PeriodTime = dialog.PeriodTime;
                        flow.Size = dialog.Size;
                    }
                }
            }
        }

        #endregion //PRZYCISKI I MENU

        #region OBŁUGA MENU
        /*
         * METODY OBSŁUGI OPERACJI Z MENU
         */
        
        //obsługa dodawania (pasów/kierunków)
        private void CrossroadAddition(TextBlock g)
        {
            var temp = g.DataContext;

            //PRZYPISZ pas wyjściowy do pasa wejściowego
            //z menu pasa wejściowego
            if (temp is Lane && IndexOfSourceLane(temp as Lane) != -1 && !(temp is PedestrianLane))
            {
                int indexSl = IndexOfSourceLane(temp as Lane);
                Direction t = _crossroad.Directions[indexSl];
                var sl = t.SourceLanes[t.SourceLanes.IndexOf(temp as Lane)];
                AddDestinationLaneDialog dialog = new AddDestinationLaneDialog(_crossroad.Directions);
                //{w celu wykluczenia zawracacnia, zamień linijki}
                //AddDestinationLaneDialog dialog = new AddDestinationLaneDialog(_crossroad.Directions, indexSL);
                if (dialog.ShowDialog() == true)
                {
                    List<Tuple<int, int>> indexesTuples = dialog.ListOfIndexes;
                    if (indexesTuples.Count > 0)
                    {
                        foreach (var tuple in indexesTuples)
                        {
                            var tt = _crossroad.Directions[tuple.Item1].DestinationLanes[tuple.Item2];
                            if (sl.DestinationsLanes.Contains(tt)) continue;
                            sl.DestinationsProbabilities.Add(tt.Name,-1);
                            sl.DestinationsLanes.Add(tt);
                        }
                    }
                    RefreshView();
                }
            }
            //dodawanie kierunku i pasa wejściowego
            else if (!(temp is Lane))
            {
                string question = GetRenameQuestion(g);
                var inputDialog = temp.GetType() == typeof (Crossroad)
                    ? new InputStringDialog(question, _crossroad.NamesList, spacesAllowed: true)
                    : new InputStringDialog(question, _crossroad.NamesList, "", true);
                if (inputDialog.ShowDialog() == true)
                {
                    string name = inputDialog.Answer;
                    //dodaj nowy kierunek do skrzyżwania
                    if (temp.GetType() == typeof(Crossroad))
                        _crossroad.AddDirection(new Direction(name));
                    //dodaj nowy pas źródłowy do kierunku
                    if (temp.GetType() == typeof (Direction))
                        //TODOne czy instert niepotrzebny?
                        //INSERT PRZED PIERWSZYM PRZEJŚCIEM DLA PIESZYCH (chyba już nie potrzebne, wystarczy sam add)
                    {
                        var t = _crossroad.Directions[_crossroad.Directions.IndexOf(temp as Direction)].SourceLanes;
                        //usuwamy przejścia
                        RemoveAllPedestiranLanes(_crossroad.Directions.IndexOf(temp as Direction));
                        t.Add(GetLane(inputDialog.TypeString, inputDialog.Answer));
                        //t.Insert(IndexOfFirstZebra(t), GetLane(inputDialog.TypeString, inputDialog.Answer));
                        //_crossroad.Directions[_crossroad.Directions.IndexOf(temp as Direction)].SourceLanes.Add(GetLane(inputDialog.TypeString, inputDialog.Answer));
                    }
                    _crossroad.AddToNameList(inputDialog.Answer);
                    RefreshView();
                }
            }
        }

        //obsługa usuwania (pasów, kierunków)
        private void CrossroadRemoval(TextBlock g)
        {
            var temp = g.DataContext;
            //USUWANIE KIERUNKU
            if (temp.GetType() == typeof (Direction))
            {
                try
                {
                    Direction dir = temp as Direction;
                    // ReSharper disable once PossibleNullReferenceException
                    RemoveNameFromNamesList(dir.Name);
                    for (int i = dir.SourceLanes.Count - 1; i >= 0; i--)
                    {
                        var l = dir.SourceLanes[i];
                        RemoveNameFromNamesList(l.Name);
                        RemoveSourceLane(l);
                    } 
                    for (int i = dir.DestinationLanes.Count - 1; i >= 0; i--)
                    {
                        var l = dir.DestinationLanes[i];
                        RemoveNameFromNamesList(l.Name);
                        RemoveDestinationLane(l);
                    }
                    _crossroad.Directions.Remove(dir);
                    RefreshView();
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            //USUWANIE PASA
            else if (temp is Lane)
            {
                Lane l = temp as Lane;
                if (temp is PedestrianLane)
                {
                    RemoveNameFromNamesList(l.Name);
                    _crossroad.Directions[IndexOfSourceLane(l)].SourceLanes.Remove(l);
                    RefreshView();
                }
                else if (l.IsSource)
                {
                    RemoveNameFromNamesList(l.Name);
                    //usuwamy przejścia
                    RemoveAllPedestiranLanes(IndexOfSourceLane(l));
                    RemoveSourceLane(l);
                    RefreshView();
                }

                //ABY USUNĄĆ pas wyjściowy tylko z pasa wejściowego, należy mieć zaznaczony dany pas wejściowy
                //ABY USUNĄĆ pas wyjściowy ze skrzyżowania, należy nie mieć nic zaznaczonego bądź zaznaczone co innego niż pas wejściowy :>
                else
                {
                    try
                    {
                        Lane lane = CrossroadTree.SelectedItem as Lane;
                        //jeśli zaiwera, to usuwamy tylko jego i potem sprawdzamy, czy nie wisi (usuwamy połączenie)
                        if (lane != null && lane.DestinationsLanes.Contains(l))
                        {
                            Direction t = _crossroad.Directions[IndexOfSourceLane(lane)];
                            t.SourceLanes[t.SourceLanes.IndexOf(lane)].DestinationsLanes.Remove(l);
                            //trzeba sprawdzić, czy nie wisi: jak wisi, to go usuwamy
                            if (DestinationLaneCounts(l) == 0)
                            {
                                RemoveNameFromNamesList(l.Name);
                                _crossroad.Directions[IndexOfDestinationLane(l)].DestinationLanes.Remove(l);
                            }
                        }
                        //w tym wypadku usuwamy go całkowicie ze skrzyżowania
                        else
                        {
                            RemoveNameFromNamesList(l.Name);
                            //usuwamy przejścia
                            RemoveAllPedestiranLanes(IndexOfSourceLane(l));
                            RemoveDestinationLane(l);
                        }
                        RefreshView();
                    }
                    catch (Exception)
                    {
                        RemoveDestinationLane(l);
                        RefreshView();
                    }
                }
            }
        }


        //usuwa pas wejściowy
        //jeśli zostałby przez to pas wyjściowy, do którego nic nie wchodzi, także go usuwa (żeby nie wisiał)
        private void RemoveSourceLane(Lane lane)
        {
            List<Lane> lanesToRemove = lane.DestinationsLanes;
            foreach (var l in lanesToRemove)
            {
                //sprawdzamy, czy po usunięciu pasa wejściowego, pas wyjściowy nie będzie wisiał
                if (DestinationLaneCounts(l) <= 1)
                {
                    RemoveNameFromNamesList(l.Name);
                    _crossroad.Directions[IndexOfDestinationLane(l)].DestinationLanes.Remove(l);
                }
            }
            _crossroad.Directions[IndexOfSourceLane(lane)].SourceLanes.Remove(lane);
        }

        //metoda do usuwania pasa wyjściowego ZE SKRZYŻOWANIA (kierunku)
        //usuwa pas ze wszystkich pasów wejściowych oraz z kierunku, do którego należy
        private void RemoveDestinationLane(Lane lane)
        {
            foreach (var dir in _crossroad.Directions)
            {
                foreach (var sl in dir.SourceLanes)
                {
                    sl.DestinationsLanes.Remove(lane);
                }
            }
            _crossroad.Directions[IndexOfDestinationLane(lane)].DestinationLanes.Remove(lane);
        }

        //usuwa wszystkie przejścia danego kierunku
        // używane w przapadku, gdy zmieniła się liczba jego pasów
        private void RemoveAllPedestiranLanes(int dirIndex)
        {
            List<Lane> lanesToRemove =
                _crossroad.Directions[dirIndex].SourceLanes.OfType<PedestrianLane>().Cast<Lane>().ToList();
            foreach (var lane in lanesToRemove)
            {
                _crossroad.AddToNameList(lane.Name);
                _crossroad.Directions[dirIndex].SourceLanes.Remove(lane);
            }
            
        }

        #endregion //OBSŁUGA MENU

        #region METODY POMOCNICZE
        /*
         * METODY POMOCNICZE
         */

        //zwraca odpowiedni obiekt w zależności od typu-string (tworzy nowy pas), do dodawania pasów z okienka
        private Lane GetLane(string typeString, string answer, bool issource = true)
        {
            switch (typeString.ToUpper())
            {
                case "TRAMWAJOWY":
                    return new TramLane(answer, issource);
                case "AUTOBUSOWY":
                    return new BusLane(answer, issource);
                case "ROWEROWY":
                    return new BicycleLane(answer, issource);
                case "PIESZY":
                    return new PedestrianLane(answer, issource);
                default:
                    return new CarLane(answer, issource);
            }
        }

        //zwraca nowy pas o nazwie i typie pasa podanego
        //b ogreśla właściwość IsSource
        public static Lane MakeLane(Lane l, bool b = true)
        {
            if (l is BicycleLane)
                return new BicycleLane(l.Name,b );
            if (l is BusLane)
                return new BusLane(l.Name,b);
            if (l is TramLane)
                return new TramLane(l.Name,b);
            return new CarLane(l.Name,b);
        }

        
        //zlicza liczbę pasów wejściowych, do których jest przypisany pas wyjściowy
        private int DestinationLaneCounts(Lane lane)
        {
            return
                _crossroad.Directions.SelectMany(dir => dir.SourceLanes)
                    .Count(sc => sc.DestinationsLanes.Contains(lane));
        }

        //indeks kieruku zawierającego dany pas wejściowy
        private int IndexOfSourceLane(Lane lane)
        {
            for (int index = 0; index < _crossroad.Directions.Count; index++)
            {
                if (_crossroad.Directions[index].SourceLanes.Contains(lane))
                    return index;
            }
            return -1;
        }

        //indeks kieruku zawierającego dany pas wyjściowy
        private int IndexOfDestinationLane(Lane lane)
        {
            for (int index = 0; index < _crossroad.Directions.Count; index++)
            {
                if (_crossroad.Directions[index].DestinationLanes.Contains(lane))
                    return index;
            }
            return -1;
        }
    
        //zwraca treść do wyświetlenia jako pytanie w oknie dialogowym
        private string GetRenameQuestion(TextBlock g)
        {
            var temp = g.DataContext;
            if (temp.GetType() == typeof(Crossroad))
                return "Podaj nazwę:";
            if (temp.GetType() == typeof(Direction))
                return "Podaj nazwę pasa źródłowego (bez spacji):";
            if (temp is Lane)
                return "Podaj nazwę pasa (bez spacji):";
            return "Nieoczekiwane pytanie";
        }

        //metoda zwraca TextBlock, którego dotyczy dane menu
        private TextBlock GetTextBlock(object sender)
        {
            MenuItem mi = sender as MenuItem;
            if (mi != null)
            {
                ContextMenu cm = mi.CommandParameter as ContextMenu;
                if (cm != null)
                    return cm.PlacementTarget as TextBlock;
            }
            return null;
        }
        
        //usuwa podany string z nazw wykorzystywanych w skrzyżowaniu
        //stosowany po zmianie nazwy bądź usunięciu pasa/kierunku
        private void RemoveNameFromNamesList(string s)
        {
            _crossroad.RemoveFromNamesList(s);
        }

        //metoda stosowana przy wyborze menuItem ustawiającego prawdopodobieństwo
        //należy mieć podświetlony pas źródłowy i wybrać menu porządanego pasa wyjściowego
        private bool HasSelectedItemGotALaneAsDestinationLane(Lane lane)
        {
            //EASTER EGG! gratulację, odkryłeś najdłuższą nazwę metody w programie!
            try
            {
                var temp = CrossroadTree.SelectedItem as Lane;
                return temp != null && temp.DestinationsLanes.Contains(lane);
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        #endregion //METODY POMOCNICZE
        #endregion //TOMASZA
    }
}